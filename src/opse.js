var opse = new Vue({
    el: '#app',

    data: {
        complexQuestionLastAnswer: "",
        npcLastReaction: "",
        simpleQuestionLastAnswer: "",
        simpleQuestionLikelyhood: 4,
        lastRolledTactics: "",
        lastRolledRoom: "",
        lastScene: "",
        
        tables: {
            scene: {
                objectives: [
                    "Conflict – a fight or action scene",
                    "Exploration – explore a dangerous location (DUNGEON CRAWLER)",
                    "Challenge – a test of skill or ability",
                    "Social – convince or trick an NPC",
                    "Travel – move through dangerous territory (random encounters)",
                    "Rest – recuperate and heal"
                ],

                spices: [
                    "An unexpected event interrupts you.",
                    "The location is different or changed.",
                    "The NPCs are new or unexpected.",
                    "An important event is already happening here.",
                    "The situation is easier or harder."
                ]
            },

            complexQuestion: {
                verbs: [
                    "Seeking", 
                    "Opposing", 
                    "Communicating", 
                    "Moving", 
                    "Harming", 
                    "Creating", 
                    "Planning",
                    "Failing",
                    "Taking",
                    "Abandoning",
                    "Assisting",
                    "Changing",
                    "Deceiving"
                ],

                objects: [
                    "[physical, strong, constructed]",
                    "[mental, plotting, technical]",
                    "[magical, intuitive, strange]",
                    "[personal, social, emotional]"
                ]
            },

            npcReaction: {
                friendly: [
                    "Talkative or gossipy",
                    "Wants to trade",
                    "Offers help or advice",
                    "Needs a favor or has a job",
                    "Has a lead or a clue",
                    "Offers direct assistance"
                ],

                neutral: [
                    "Not interested in talking",
                    "Wants to trade",
                    "Requests tribute or payment",
                    "Needs a favor or has a job",
                    "Tries to trick or deceive",
                    "Pursuing unrelated objective"
                ],

                hostile: [
                    "Attacks without warning",
                    "Threatens or harasses",
                    "Demands tribute or payment",
                    "Denies access",
                    "Tries to trick or deceive",
                    "Pursuing counter objective"
                ]
            },
            enemy: {
                tactics: [
                    "Attack recklessly for max damage",
                    "Aid an ally or heal",
                    "Act according to this unit’s role",
                    "Take a defensive posture",
                    "Seek an advantage",
                    "Focus on a weak target"
                ]
            },

            dungeonCrawl: {
                locations: [
                    "A living area or meeting place",
                    "A working or utility area",
                    "A typical, unremarkable area",
                    "A typical, unremarkable area",
                    "An area with a special feature",
                    "A location for a specialized purpose"
                ],

                encounters: [
                    "Hostile enemies",
                    "Hostile enemies",
                    "None",
                    "None",
                    "A friendly or neutral NPC",
                    "A unique NPC or adversary"
                ],

                objects: [
                    "An interesting item or clue",
                    "A useful tool, key, or device",
                    "Nothing, or mundane objects",
                    "Nothing, or mundane objects",
                    "A valuable treasure",
                    "A rare or special item"
                ],

                specials: [
                    "There’s a trap here",
                    "There’s a secret hidden here",
                    "Nothing special",
                    "Nothing special",
                    "Nothing special",
                    "A challenge or item is enhanced"
                ],

                exits: [
                    "Dead end (1 exit)",
                    "2 exits",
                    "2 exits",
                    "3 exits",
                    "3 exits (connects to existing area)",
                    "3 exits (50% for dungeon exit)"
                ]
            }
        }
    },

    methods: {
        rollD: function(faces) {
            return this.getRandomInt(1, faces);
        },

        getRandomInt: function(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        },

        getRandomEntry: function(table){
            var min = 0;
            var max = table.length - 1;
            var roll = this.getRandomInt(min, max);

            return table[roll];
        },

        setTheScene: function(){
            var scene = this.getRandomEntry(this.tables.scene.objectives);

            if (this.rollD(6) == 6) {
                scene += ". " + this.getRandomEntry(this.tables.scene.spices);
            }

            return scene;
        },

        askSimpleQuestion: function(yesOn){
            var answer = this.rollD(6) >= yesOn ? "Yes" : "No";

            var qualifier = this.rollD(6);
            if (qualifier == 1) {
                answer += ", but...";
            }
            else if (qualifier == 6){
                answer += ", and...";   
            }
            else {
                answer += "."
            }
            return answer;
        },

        askComplexQuestion: function(){
            return this.getRandomEntry(this.tables.complexQuestion.verbs) + " " + this.getRandomEntry(this.tables.complexQuestion.objects);
        },

        checkNpcReaction : function (){
 

            var reactionTypeRoll = this.rollD(3);
            var reaction = ""
            
            if (reactionTypeRoll == 1) {
                reaction = "[Friendly] " + this.getRandomEntry(this.tables.npcReaction.friendly);
            }
            else if (reactionTypeRoll == 2) {
                reaction = "[Neutral] " + this.getRandomEntry(this.tables.npcReaction.neutral);
            }
            else {
                reaction = "[Hostile] " + this.getRandomEntry(this.tables.npcReaction.hostile);
            }
            return reaction;
        },

        checkEnemyTactics: function (){
            var tactics = this.lastRolledTactics

            if (this.lastRolledTactics == "" || this.rollD(6) >= 5){
                tactics = this.getRandomEntry(this.tables.enemy.tactics);
            }
            
            return tactics;
        },

        dungeonCrawl: function(){
            var area = "Location:  " + this.getRandomEntry(this.tables.dungeonCrawl.locations);
            area += ". Encounter: " + this.getRandomEntry(this.tables.dungeonCrawl.encounters)
            area += ". Object: " + this.getRandomEntry(this.tables.dungeonCrawl.objects);
            area += ". Special: " + this.getRandomEntry(this.tables.dungeonCrawl.specials);

            if (this.lastRolledRoom == ""){
                area += ". Exits: 3 exits";
            }
            else {
                area += ". Exits: " + this.getRandomEntry(this.tables.dungeonCrawl.exits);
            }
            return area;

        }
    }
})
